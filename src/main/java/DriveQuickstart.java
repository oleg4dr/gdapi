import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


public class DriveQuickstart {

    private static final String APPLICATION_NAME = "Google Drive API Java Quickstart";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */

    //set app permissions
    //private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE, DriveScopes.DRIVE_FILE);
    private static final List<String> SCOPES = Arrays.asList(DriveScopes.DRIVE, DriveScopes.DRIVE_FILE, DriveScopes.DRIVE_APPDATA, DriveScopes.DRIVE_SCRIPTS, DriveScopes.DRIVE_METADATA);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = DriveQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    public static void listFiles(Drive service) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("How many files from disk you want to see?");
        int X = sc.nextInt();
        FileList result = service.files().list()
                .setQ("'174rEW8IwS2z7V8fIIcwj3aDnDz7TPnIP' in parents")
                //1kQF47MWtuqZeEuCQ_mycxNfYUJMSNmvu - папка ТРПП
                //"mimeType = 'application/vnd.google-apps.folder'" - поиск папок
                //'id' in parents - поиск в папке
                .setSpaces("drive")
                .setPageSize(X)
                .setFields("nextPageToken, files(id, name)")
                .execute();
        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
            System.out.println("Files:");
            for (File file : files) {
                System.out.printf("%s (%s)\n", file.getName(), file.getId());
            }
        }
    }

    public static void deleteFile(Drive service) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("input file id");
        String id = sc.nextLine();
        service.files().delete(id).execute();
        System.out.println("File with id: " + id + "was successfully deleted");
    }

    public static void uploadFile(Drive service) throws IOException {
        String folderId = "1kQF47MWtuqZeEuCQ_mycxNfYUJMSNmvu";
        File fileMetadata = new File();
        fileMetadata.setName("photo4.jpg");
        fileMetadata.setParents(Collections.singletonList(folderId));
        java.io.File filePath = new java.io.File("./src/main/resources/roo44.png");
        FileContent mediaContent = new FileContent("image/jpeg", filePath);
        File file = service.files().create(fileMetadata, mediaContent)
                .setFields("id, parents")
                .execute();
        System.out.println("Uploaded file ID: " + file.getId());
    }

    public static void update(Drive service) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("input file id");
        String id = sc.nextLine();
        File fileMetadata = new File();
        fileMetadata.setName("photo4.jpg");
        java.io.File filePath = new java.io.File("./src/main/resources/rooSlain.png");
        FileContent mediaContent = new FileContent("image/jpeg", filePath);
        File file = service.files().update(id, fileMetadata, mediaContent)
                .execute();
        System.out.println("Updated file with ID: " + file.getId());
    }




    public static void main(String... args) throws IOException, GeneralSecurityException {
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        boolean lock = true;
        Scanner sc = new Scanner(System.in);
        System.out.println("\t DRIVE SIMPLE CRUD\n1 - view\n2 - upload file\n3 - delete file\n4 - update file\n0 - exit");
        while (lock)
        {
            int input = sc.nextInt();
            switch (input){
                case 1: listFiles(service); break;
                case 2: uploadFile(service); break;
                case 3: deleteFile(service); break;
                case 4: update(service); break;
                case 0: System.out.println("End"); lock=false; break;
                default: System.out.println("command not recognized"); break;
            }
        }
    }
}